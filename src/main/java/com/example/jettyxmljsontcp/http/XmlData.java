package com.example.jettyxmljsontcp.http;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class XmlData {
    private String data;
    private String parsingError;
    private String sendingError;
    private String successMarshallingToJSON;
}
