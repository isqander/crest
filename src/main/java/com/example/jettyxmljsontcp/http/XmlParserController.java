package com.example.jettyxmljsontcp.http;

import com.example.jettyxmljsontcp.services.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class XmlParserController {
    @Autowired
    private DataService dataService;

    @GetMapping("/input")
    public String greetingForm(Model model) {
        XmlData xmlData = new XmlData();
        xmlData.setData("data");
        model.addAttribute("xmlData", xmlData);
        return "input";
    }

    @PostMapping("/input")
    public String greetingSubmit(@ModelAttribute XmlData xmlData) {
        xmlData = dataService.handleXML(xmlData);
        return "result";
    }


}
