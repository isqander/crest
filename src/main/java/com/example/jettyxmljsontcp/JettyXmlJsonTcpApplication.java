package com.example.jettyxmljsontcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JettyXmlJsonTcpApplication {

    public static void main(String[] args) {
        SpringApplication.run(JettyXmlJsonTcpApplication.class, args);
    }

}
