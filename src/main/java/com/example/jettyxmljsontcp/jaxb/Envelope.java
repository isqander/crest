package com.example.jettyxmljsontcp.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "envelope")
@XmlRootElement(name = "Holder")
public class Envelope {
    @XmlElement (name="Body")
    public Body body;
}
