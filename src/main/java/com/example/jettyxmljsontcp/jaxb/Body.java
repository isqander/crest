package com.example.jettyxmljsontcp.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Body {
    @XmlElement(name = "SendStat", type = SendStat.class, namespace = "cwtapi:Stat")
    public SendStat sendStat;
}
