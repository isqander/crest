package com.example.jettyxmljsontcp.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

@XmlType
public class SendStat {
    @XmlElement
    public String requestId;
    @XmlElement
    public String status;
    @XmlElement(namespace = "cwtapi:Spatial")
    public ArrayList<Station> station;
}
