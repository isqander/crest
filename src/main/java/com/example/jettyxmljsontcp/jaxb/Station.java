package com.example.jettyxmljsontcp.jaxb;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "station")
public class Station {
    @XmlAttribute
    public String lat;
    @XmlAttribute
    public String lon;
    @XmlAttribute
    public String elev;
}
