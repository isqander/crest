package com.example.jettyxmljsontcp.services;

import com.example.jettyxmljsontcp.http.XmlData;
import com.example.jettyxmljsontcp.jaxb.Envelope;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.Socket;

@Service
public class DataService {
    @Value("${tcp.dest.addr}")
    String serverAddress;
    @Value("${tcp.dest.port}")
    int serverPort;
    @Autowired
    XmlData dataBean;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public XmlData handleXML(XmlData xmlData) {
        dataBean = xmlData;
        Envelope envelope = unmarshallXML();
        if (envelope != null) {
            String json = marshallToJSON(envelope);
            sendToSocket(json);
        }
        return dataBean;
    }

    private Envelope unmarshallXML() {
        Envelope envelope = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Envelope.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            envelope = (Envelope) jaxbUnmarshaller.unmarshal(new StringReader(dataBean.getData()));
        } catch (JAXBException e) {
            logger.error(e.toString());
            dataBean.setParsingError("An exception occurred while parsing XML:  " + e.toString());
        }
        return envelope;
    }

    private String marshallToJSON(Envelope envelope){
        ObjectMapper objectMapper = new ObjectMapper();
        String json = null;
        try {
            json = objectMapper.writeValueAsString(envelope);
            dataBean.setSuccessMarshallingToJSON(json);
        } catch (IOException e) {
            logger.error(e.toString());
        }
        logger.info("The data marshaled to JSON:\n" + json);
        return json;
    }

    private void sendToSocket(String json) {
        try {
            logger.info("Trying to send data:\n" + encodeHexString(json.getBytes()));
            Socket socket = new Socket(serverAddress, serverPort);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(json);
        } catch (IOException e) {
            logger.error(e.toString());
            dataBean.setSendingError("An exception occurred while sending data to socket:  " + e.toString());
        }
    }

    public String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }
}
